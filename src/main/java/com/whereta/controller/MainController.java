package com.whereta.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by vincent on 15-10-9.
 */
@Controller
public class MainController {

    //首页
    @RequestMapping("/index")
    public String index(){
        return "index";
    }
}
