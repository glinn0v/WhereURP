package com.whereta.dao;

import com.whereta.model.Permission;

import java.util.List;

/**
 * @author Vincent
 * @time 2015/8/27 17:30
 */
public interface IPermissionDao {

    Permission get(List<Permission> permissionList, int id);

    List<Permission> selectAll();

    int createPermission(Permission permission);

    int deletePermission(int perId);

    int updatePermission(Permission permission);
}
