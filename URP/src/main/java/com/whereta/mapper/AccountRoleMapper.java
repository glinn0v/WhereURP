package com.whereta.mapper;

import com.whereta.model.AccountRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component("accountRoleMapper")
public interface AccountRoleMapper {
    /**
     * 根据主键删除
     * 参数:主键
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入，空属性也会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int insert(AccountRole record);

    /**
     * 插入，空属性不会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int insertSelective(AccountRole record);

    /**
     * 根据主键查询
     * 参数:查询条件,主键值
     * 返回:对象
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    AccountRole selectByPrimaryKey(Integer id);

    /**
     * 根据主键修改，空值条件不会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int updateByPrimaryKeySelective(AccountRole record);

    /**
     * 根据主键修改，空值条件会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int updateByPrimaryKey(AccountRole record);

    Set<Integer> selectRoleIdSet(@Param("accountId") int accountId);

    int delete(@Param("roleId") Integer roleId, @Param("accountId") Integer accountId);
}